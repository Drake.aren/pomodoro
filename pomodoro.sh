#!/bin/sh

WORK_TIME=1500
WORK_TIME_MULT=1.5

SHORT_BREAK=300
SHORT_BREAK_MULT=2

LONG_BREAK=900
LONG_BREAK_MULT=2

start_timestamp=0
end_timestamp=0
pomodoro_count=0

#let "WORK_TIME=$WORK_TIME * $WORK_TIME_MULT"
#let "SHORT_BREAK=$SHORT_BREAK * $SHORT_BREAK_MULT"
#let "LONG_BREAK=$LONG_BREAK * $LONG_BREAK_MULT"

function timestamp {
    date +%s
}

function timestampToDate {
    date -d @$1
}

function breakWork {
    case $1 in
        "long")
            notify-send "Long break"
            echo "$(date) : Long break"
            sleep $LONG_BREAK
        ;;
        "short")
            notify-send "Break"
            echo "$(date) : Break"
            sleep $SHORT_BREAK
        ;;
    esac
    let "pomodoro_count+=1"
    let "time_spent+=timestamp"
}

function startWork {
  start_timestamp=$(timestamp)
  echo "$(date) : Start work"
  sleep $WORK_TIME
}

function backToWork {
  notify-send "Back to work"
  echo "$(date) : Back to work"
  sleep $WORK_TIME
}

function ctrl_c {
  end_timestamp=$(timestamp)
  echo "
Pomodoro count : $pomodoro_count
Time spent     : $(( $end_timestamp - $start_timestamp ))"
  exit 0
}

trap ctrl_c INT

echo "<<[PoMoDoRo]>>"

startWork

for ((;;))
do
  for (( i=0; i < 3; i++ ))
  do
    breakWork "short"
    backToWork
  done
  sleep $WORK_TIME
  breakWork "long"
  backToWork
done

